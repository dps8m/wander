#DOT_CS	= wand1.c wand2.c wandglb.c wandsys.c
#DOT_OS	= wand1.cob wand2.cob wandglb.cob wandsys.cob
DOC_PL1S  = wandsys.pl1
DOT_OS	= wand1.cob wand2.cob wandglb.cob wandsys
A3	= a3.misc a3.wrld
CASTLE	= castle.misc castle.wrld
LIBRARY	= library.misc library.wrld
TUT	= tut.misc tut.wrld

all: Wander

clean:
	-rm Wander

Wander: $(DOT_OS) wanddef.h
	cc $(DOT_OS) -of $@

wand1.cob : wand1.c wanddef.h
	cc wand1.c -spaf alm

wand2.cob : wand2.c wanddef.h
	cc wand2.c -spaf alm

wandglb.cob : wandglb.c wanddef.h
	cc wandglb.c -spaf alm

wandsys.cob : wandsys.c wanddef.h
	cc wandsys.c -spaf alm

wandsys : wandsys.pl1
	pl1 wandsys
